FROM golang:1.22-alpine AS builder

WORKDIR /device-mgmt

COPY go.mod go.sum ./
RUN go mod download

COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -o deviceapi

FROM alpine:latest

RUN apk --no-cache add ca-certificates

WORKDIR /device-mgmt

COPY --from=builder /device-mgmt/deviceapi .

EXPOSE 8080

CMD ["./deviceapi"]
