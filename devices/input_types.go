package devices

type CreateDeviceInput struct {
	Name  string `json:"name" binding:"required"`
	Brand string `json:"brand" binding:"required"`
}

type UpdateDeviceInput struct {
	Name  string `json:"name" binding:"required"`
	Brand string `json:"brand" binding:"required"`
}

type PatchDeviceInput struct {
	Name  *string `json:"name,omitempty"`
	Brand *string `json:"brand,omitempty"`
}

type GetDeviceByID struct {
	ID string `json:"id" uri:"id"`
}

type GetManyDevices struct {
	Limit  int32 `json:"limit" query:"limit"`
	Offset int32 `json:"offset" query:"offset"`
}

type GetManyByBrand struct {
	Brand  string `json:"brand" uri:"brand"`
	Limit  int32  `json:"limit" query:"limit"`
	Offset int32  `json:"offset" query:"offset"`
}
