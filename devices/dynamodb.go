package devices

import (
	"context"
	"errors"
	"math"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"github.com/google/uuid"
)

type DynamoDBStore struct {
	client *dynamodb.Client
}

func NewDynamoDBStore(client *dynamodb.Client) Store {
	return &DynamoDBStore{client: client}
}

func (d *DynamoDBStore) EnsureTable(ctx context.Context) error {
	if d.client == nil {
		return NewDatabaseError("client is nil")
	}

	// Check if table exists
	_, err := d.client.DescribeTable(ctx, &dynamodb.DescribeTableInput{
		TableName: aws.String("Devices"),
	})

	if err != nil {
		var notFoundErr *types.ResourceNotFoundException
		if errors.As(err, &notFoundErr) {
			// Table doesn't exist, create it
			input := &dynamodb.CreateTableInput{
				AttributeDefinitions: []types.AttributeDefinition{
					{
						AttributeName: aws.String("ID"),
						AttributeType: types.ScalarAttributeTypeS,
					},
					{
						AttributeName: aws.String("Brand"),
						AttributeType: types.ScalarAttributeTypeS,
					},
				},
				KeySchema: []types.KeySchemaElement{
					{
						AttributeName: aws.String("ID"),
						KeyType:       types.KeyTypeHash,
					},
				},
				GlobalSecondaryIndexes: []types.GlobalSecondaryIndex{
					{
						IndexName: aws.String("BrandIndex"),
						KeySchema: []types.KeySchemaElement{
							{
								AttributeName: aws.String("Brand"),
								KeyType:       types.KeyTypeHash,
							},
						},
						Projection: &types.Projection{
							ProjectionType: types.ProjectionTypeAll,
						},
						ProvisionedThroughput: &types.ProvisionedThroughput{
							ReadCapacityUnits:  aws.Int64(5),
							WriteCapacityUnits: aws.Int64(5),
						},
					},
				},
				ProvisionedThroughput: &types.ProvisionedThroughput{
					ReadCapacityUnits:  aws.Int64(5),
					WriteCapacityUnits: aws.Int64(5),
				},
				TableName: aws.String("Devices"),
			}

			_, err = d.client.CreateTable(ctx, input)
			if err != nil {
				return NewDatabaseError("failed to create table: " + err.Error())
			}

			waiter := dynamodb.NewTableExistsWaiter(d.client)
			if err = waiter.Wait(ctx, &dynamodb.DescribeTableInput{}, 5*time.Second); err != nil {
				return NewDatabaseError("error waiting for table to be active: " + err.Error())
			}
			// Wait for the table to be active
		} else {
			return NewDatabaseError("failed to describe table: " + err.Error())
		}
	}

	return nil
}

func (d *DynamoDBStore) AddDevice(ctx context.Context, input *CreateDeviceInput) (*Device, error) {
	device := &Device{
		ID:        uuid.New().String(),
		Brand:     input.Brand,
		Name:      input.Name,
		CreatedAt: time.Now(),
	}

	av, err := attributevalue.MarshalMap(device)
	if err != nil {
		return nil, NewDatabaseError("failed to marshal device: " + err.Error())
	}

	_, err = d.client.PutItem(ctx, &dynamodb.PutItemInput{
		TableName: aws.String("Devices"),
		Item:      av,
	})
	if err != nil {
		return nil, NewDatabaseError("failed to add device: " + err.Error())
	}

	return device, nil
}

func (d *DynamoDBStore) GetDevice(ctx context.Context, id string) (*Device, error) {
	result, err := d.client.GetItem(ctx, &dynamodb.GetItemInput{
		TableName: aws.String("Devices"),
		Key: map[string]types.AttributeValue{
			"ID": &types.AttributeValueMemberS{Value: id},
		},
	})
	if err != nil {
		return nil, NewDatabaseError("failed to get device: " + err.Error())
	}

	if result.Item == nil {
		return nil, NewNotFoundError("device not found")
	}

	device := &Device{}
	err = attributevalue.UnmarshalMap(result.Item, device)
	if err != nil {
		return nil, NewDatabaseError("failed to unmarshal device: " + err.Error())
	}

	return device, nil
}

func (d *DynamoDBStore) ListDevices(ctx context.Context, params PaginationParams) (*PaginatedResult, error) {
	// First, get the total count
	countOutput, err := d.client.Scan(ctx, &dynamodb.ScanInput{
		TableName: aws.String("Devices"),
		Select:    types.SelectCount,
	})
	if err != nil {
		return nil, NewDatabaseError("failed to get total count: " + err.Error())
	}
	totalCount := countOutput.Count

	// Set up the scan input
	scanInput := &dynamodb.ScanInput{
		TableName: aws.String("Devices"),
		Limit:     aws.Int32(params.Limit),
	}

	// Create the paginator
	paginator := dynamodb.NewScanPaginator(d.client, scanInput)

	var devices []Device
	pageNum := int32(0)
	for paginator.HasMorePages() && pageNum*params.Limit < params.Offset+params.Limit {
		page, err := paginator.NextPage(ctx)
		if err != nil {
			return nil, NewDatabaseError("failed to get page: " + err.Error())
		}

		// If we've reached the desired offset, start collecting items
		if pageNum*params.Limit >= params.Offset {
			var pageDevices []Device
			err = attributevalue.UnmarshalListOfMaps(page.Items, &pageDevices)
			if err != nil {
				return nil, NewDatabaseError("failed to unmarshal devices: " + err.Error())
			}
			devices = append(devices, pageDevices...)
		}

		pageNum++
	}

	// Trim any extra items if we've collected more than the limit
	if len(devices) > int(params.Limit) {
		devices = devices[:params.Limit]
	}

	currentPage := (params.Offset / params.Limit) + 1
	totalPages := int32(math.Ceil(float64(totalCount) / float64(params.Limit)))

	return &PaginatedResult{
		Devices:     devices,
		TotalCount:  totalCount,
		CurrentPage: currentPage,
		TotalPages:  totalPages,
	}, nil
}

func (d *DynamoDBStore) UpdateDevice(ctx context.Context, id string, input *UpdateDeviceInput) (*Device, error) {
	_, err := d.client.UpdateItem(ctx, &dynamodb.UpdateItemInput{
		TableName: aws.String("Devices"),
		Key: map[string]types.AttributeValue{
			"ID": &types.AttributeValueMemberS{Value: id},
		},
		UpdateExpression: aws.String("SET #name = :name, #brand = :brand"),
		ExpressionAttributeNames: map[string]string{
			"#name":  "Name",
			"#brand": "Brand",
		},
		ExpressionAttributeValues: map[string]types.AttributeValue{
			":name":  &types.AttributeValueMemberS{Value: input.Name},
			":brand": &types.AttributeValueMemberS{Value: input.Brand},
		},
		ConditionExpression: aws.String("attribute_exists(ID)"),
		ReturnValues:        types.ReturnValueAllNew,
	})
	if err != nil {
		var cfe *types.ConditionalCheckFailedException
		if errors.As(err, &cfe) {
			return nil, NewNotFoundError("device not found")
		}
		return nil, NewDatabaseError("failed to update device: " + err.Error())
	}

	// Fetch the updated device
	return d.GetDevice(ctx, id)
}

func (d *DynamoDBStore) PatchDevice(ctx context.Context, id string, input *PatchDeviceInput) (*Device, error) {
	updateExpression := "SET "
	expressionAttributeNames := map[string]string{}
	expressionAttributeValues := map[string]types.AttributeValue{}

	if input.Name != nil {
		updateExpression += "#name = :name, "
		expressionAttributeNames["#name"] = "Name"
		expressionAttributeValues[":name"] = &types.AttributeValueMemberS{Value: *input.Name}
	}
	if input.Brand != nil {
		updateExpression += "#brand = :brand, "
		expressionAttributeNames["#brand"] = "Brand"
		expressionAttributeValues[":brand"] = &types.AttributeValueMemberS{Value: *input.Brand}
	}

	// Remove trailing comma and space
	updateExpression = updateExpression[:len(updateExpression)-2]

	_, err := d.client.UpdateItem(ctx, &dynamodb.UpdateItemInput{
		TableName: aws.String("Devices"),
		Key: map[string]types.AttributeValue{
			"ID": &types.AttributeValueMemberS{Value: id},
		},
		UpdateExpression:          aws.String(updateExpression),
		ExpressionAttributeNames:  expressionAttributeNames,
		ExpressionAttributeValues: expressionAttributeValues,
		ConditionExpression:       aws.String("attribute_exists(ID)"),
		ReturnValues:              types.ReturnValueAllNew,
	})
	if err != nil {
		var cfe *types.ConditionalCheckFailedException
		if errors.As(err, &cfe) {
			return nil, NewNotFoundError("device not found")
		}
		return nil, NewDatabaseError("failed to update device: " + err.Error())
	}

	// Fetch the updated device
	return d.GetDevice(ctx, id)
}

func (d *DynamoDBStore) DeleteDevice(ctx context.Context, id string) error {
	_, err := d.client.DeleteItem(ctx, &dynamodb.DeleteItemInput{
		TableName: aws.String("Devices"),
		Key: map[string]types.AttributeValue{
			"ID": &types.AttributeValueMemberS{Value: id},
		},
		ConditionExpression: aws.String("attribute_exists(ID)"),
	})
	if err != nil {
		var cfe *types.ConditionalCheckFailedException
		if errors.As(err, &cfe) {
			return NewNotFoundError("device not found")
		}
		return NewDatabaseError("failed to delete device: " + err.Error())
	}

	return nil
}

func (d *DynamoDBStore) SearchDevicesByBrand(ctx context.Context, brand string, params PaginationParams) (*PaginatedResult, error) {
	// Set up the query input
	queryInput := &dynamodb.QueryInput{
		TableName:              aws.String("Devices"),
		IndexName:              aws.String("BrandIndex"),
		KeyConditionExpression: aws.String("#brand = :brand"),
		ExpressionAttributeNames: map[string]string{
			"#brand": "Brand",
		},
		ExpressionAttributeValues: map[string]types.AttributeValue{
			":brand": &types.AttributeValueMemberS{Value: brand},
		},
		Limit: aws.Int32(params.Limit),
	}

	// Create the paginator
	paginator := dynamodb.NewQueryPaginator(d.client, queryInput)

	var devices []Device
	var totalCount int32
	pageNum := int32(0)
	for paginator.HasMorePages() && pageNum*params.Limit < params.Offset+params.Limit {
		page, err := paginator.NextPage(ctx)
		if err != nil {
			return nil, NewDatabaseError("failed to get page: " + err.Error())
		}

		totalCount += page.Count

		// If we've reached the desired offset, start collecting items
		if pageNum*params.Limit >= params.Offset {
			var pageDevices []Device
			err = attributevalue.UnmarshalListOfMaps(page.Items, &pageDevices)
			if err != nil {
				return nil, NewDatabaseError("failed to unmarshal devices: " + err.Error())
			}
			devices = append(devices, pageDevices...)
		}

		pageNum++
	}

	// Trim any extra items if we've collected more than the limit
	if len(devices) > int(params.Limit) {
		devices = devices[:params.Limit]
	}

	currentPage := (params.Offset / params.Limit) + 1
	totalPages := int32(math.Ceil(float64(totalCount) / float64(params.Limit)))

	return &PaginatedResult{
		Devices:     devices,
		TotalCount:  totalCount,
		CurrentPage: currentPage,
		TotalPages:  totalPages,
	}, nil
}
