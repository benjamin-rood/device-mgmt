package devices

import (
	"context"
)

type DeviceService struct {
	store Store
}

func NewDeviceService(store Store) *DeviceService {
	return &DeviceService{store: store}
}

func (s *DeviceService) CreateDevice(ctx context.Context, input CreateDeviceInput) (*Device, error) {
	return s.store.AddDevice(ctx, &input)
}

func (s *DeviceService) GetDevice(ctx context.Context, id string) (*Device, error) {
	return s.store.GetDevice(ctx, id)
}

func (s *DeviceService) ListDevices(ctx context.Context, params PaginationParams) (*PaginatedResult, error) {
	return s.store.ListDevices(ctx, params)
}

func (s *DeviceService) UpdateDevice(ctx context.Context, id string, input UpdateDeviceInput) (*Device, error) {
	return s.store.UpdateDevice(ctx, id, &input)
}

func (s *DeviceService) PatchDevice(ctx context.Context, id string, input PatchDeviceInput) (*Device, error) {
	return s.store.PatchDevice(ctx, id, &input)
}

func (s *DeviceService) DeleteDevice(ctx context.Context, id string) error {
	return s.store.DeleteDevice(ctx, id)
}

func (s *DeviceService) SearchDevicesByBrand(ctx context.Context, brand string, pagination PaginationParams) (*PaginatedResult, error) {
	return s.store.SearchDevicesByBrand(ctx, brand, pagination)
}
