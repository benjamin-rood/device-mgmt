package devices

import (
	"errors"
	"fmt"
)

var (
	NotFoundError = errors.New("NOT_FOUND")
	DatabaseError = errors.New("DATABASE")
)

func NewDatabaseError(message string) error {
	return fmt.Errorf("%w: %s", DatabaseError, message)
}

func NewNotFoundError(message string) error {
	return fmt.Errorf("%w: %s", NotFoundError, message)
}
