package devices

import (
	"context"
	"time"
)

type Device struct {
	ID        string    `json:"id"`
	Brand     string    `json:"brand"`
	Name      string    `json:"name"`
	CreatedAt time.Time `json:"created_at"`
}

type PaginationParams struct {
	Limit  int32
	Offset int32
}

type PaginatedResult struct {
	Devices     []Device `json:"devices"`
	TotalCount  int32    `json:"total_count"`
	CurrentPage int32    `json:"current_page"`
	TotalPages  int32    `json:"total_pages"`
}

type Store interface {
	EnsureTable(ctx context.Context) error
	AddDevice(ctx context.Context, input *CreateDeviceInput) (*Device, error)
	GetDevice(ctx context.Context, id string) (*Device, error)
	ListDevices(ctx context.Context, params PaginationParams) (*PaginatedResult, error)
	UpdateDevice(ctx context.Context, id string, input *UpdateDeviceInput) (*Device, error)
	PatchDevice(ctx context.Context, id string, input *PatchDeviceInput) (*Device, error)
	DeleteDevice(ctx context.Context, id string) error
	SearchDevicesByBrand(ctx context.Context, brand string, pagination PaginationParams) (*PaginatedResult, error)
}
