//go:build integration
// +build integration

package devices

import (
	"context"
	"testing"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestDeviceService(t *testing.T) {
	cfg, err := config.LoadDefaultConfig(context.Background())
	if err != nil {
		t.Fatalf("Failed to load configuration: %v", err)
	}

	dynamoClient := dynamodb.NewFromConfig(cfg, func(o *dynamodb.Options) {
		o.Region = "eu-west-1"
		o.BaseEndpoint = aws.String("http://localhost:8000")
	})

	store := NewDynamoDBStore(dynamoClient)
	service := NewDeviceService(store)

	// Ensure the table exists
	err = store.EnsureTable(context.Background())
	require.NoError(t, err, "Failed to ensure table exists")

	t.Run("TestCreateDevice", func(t *testing.T) {
		ctx := context.Background()
		input := CreateDeviceInput{
			Brand: "TestBrand",
			Name:  "TestDevice",
		}

		device, err := service.CreateDevice(ctx, input)
		require.NoError(t, err, "Failed to create device")
		assert.NotEmpty(t, device.ID, "Device ID should not be empty")
		assert.Equal(t, input.Brand, device.Brand, "Device brand should match input")
		assert.Equal(t, input.Name, device.Name, "Device name should match input")
		assert.WithinDuration(t, time.Now(), device.CreatedAt, 5*time.Second, "CreatedAt should be recent")
	})

	t.Run("TestGetDevice", func(t *testing.T) {
		ctx := context.Background()
		input := CreateDeviceInput{
			Brand: "TestBrand",
			Name:  "TestDevice",
		}

		createdDevice, err := service.CreateDevice(ctx, input)
		require.NoError(t, err, "Failed to create device")

		retrievedDevice, err := service.GetDevice(ctx, createdDevice.ID)
		require.NoError(t, err, "Failed to get device")
		assert.Equal(t, createdDevice.ID, retrievedDevice.ID, "Retrieved device ID should match created device")
		assert.Equal(t, createdDevice.Brand, retrievedDevice.Brand, "Retrieved device brand should match created device")
		assert.Equal(t, createdDevice.Name, retrievedDevice.Name, "Retrieved device name should match created device")
	})

	t.Run("TestGetNonExistentDevice", func(t *testing.T) {
		ctx := context.Background()
		_, err := service.GetDevice(ctx, "non-existent-id")
		assert.Error(t, err, "Getting non-existent device should return an error")
		assert.ErrorIs(t, err, NotFoundError, "Error should be of type NotFoundError")
	})

	t.Run("TestUpdateDevice", func(t *testing.T) {
		ctx := context.Background()
		input := CreateDeviceInput{
			Brand: "TestBrand",
			Name:  "TestDevice",
		}

		createdDevice, err := service.CreateDevice(ctx, input)
		require.NoError(t, err, "Failed to create device")

		updateInput := UpdateDeviceInput{
			Brand: "UpdatedBrand",
			Name:  "UpdatedDevice",
		}

		updatedDevice, err := service.UpdateDevice(ctx, createdDevice.ID, updateInput)
		require.NoError(t, err, "Failed to update device")
		assert.Equal(t, createdDevice.ID, updatedDevice.ID, "Updated device ID should match original")
		assert.Equal(t, updateInput.Brand, updatedDevice.Brand, "Updated device brand should match input")
		assert.Equal(t, updateInput.Name, updatedDevice.Name, "Updated device name should match input")
	})

	t.Run("TestUpdateNonExistentDevice", func(t *testing.T) {
		ctx := context.Background()
		updateInput := UpdateDeviceInput{
			Brand: "UpdatedBrand",
			Name:  "UpdatedDevice",
		}
		_, err := service.UpdateDevice(ctx, "non-existent-id", updateInput)
		assert.Error(t, err, "Updating non-existent device should return an error")
		assert.ErrorIs(t, err, NotFoundError, "Error should be of type NotFoundError")
	})

	t.Run("TestPatchDevice", func(t *testing.T) {
		ctx := context.Background()

		// First, create a device
		createInput := CreateDeviceInput{
			Brand: "OriginalBrand",
			Name:  "OriginalDevice",
		}
		createdDevice, err := service.CreateDevice(ctx, createInput)
		require.NoError(t, err, "Failed to create device")

		// Round the CreatedAt time to the nearest second
		createdAtRounded := createdDevice.CreatedAt.Round(time.Second)

		// Now, let's patch it
		newName := "UpdatedDevice"
		patchInput := PatchDeviceInput{
			Name: &newName,
		}

		updatedDevice, err := service.PatchDevice(ctx, createdDevice.ID, patchInput)
		require.NoError(t, err, "Failed to patch device")

		// Round the updated CreatedAt time to the nearest second
		updatedAtRounded := updatedDevice.CreatedAt.Round(time.Second)

		// Check that only the patched field was updated
		assert.Equal(t, createdDevice.ID, updatedDevice.ID, "Device ID should not change")
		assert.Equal(t, createdDevice.Brand, updatedDevice.Brand, "Brand should not change")
		assert.Equal(t, newName, updatedDevice.Name, "Name should be updated")
		assert.Equal(t, createdAtRounded, updatedAtRounded, "CreatedAt should not change (rounded to nearest second)")

		// Now let's patch the brand
		newBrand := "UpdatedBrand"
		patchInput = PatchDeviceInput{
			Brand: &newBrand,
		}

		updatedDevice, err = service.PatchDevice(ctx, createdDevice.ID, patchInput)
		require.NoError(t, err, "Failed to patch device")

		// Round the updated CreatedAt time again
		updatedAtRounded = updatedDevice.CreatedAt.Round(time.Second)

		// Check that only the brand was updated this time
		assert.Equal(t, createdDevice.ID, updatedDevice.ID, "Device ID should not change")
		assert.Equal(t, newBrand, updatedDevice.Brand, "Brand should be updated")
		assert.Equal(t, newName, updatedDevice.Name, "Name should not change")
		assert.Equal(t, createdAtRounded, updatedAtRounded, "CreatedAt should not change (rounded to nearest second)")
	})

	t.Run("TestPatchNonExistentDevice", func(t *testing.T) {
		ctx := context.Background()
		newName := "UpdatedDevice"
		patchInput := PatchDeviceInput{
			Name: &newName,
		}

		_, err := service.PatchDevice(ctx, "non-existent-id", patchInput)
		assert.Error(t, err, "Patching non-existent device should return an error")
		assert.ErrorIs(t, err, NotFoundError, "Error should be of type NotFoundError")
	})

	t.Run("TestDeleteDevice", func(t *testing.T) {
		ctx := context.Background()
		input := CreateDeviceInput{
			Brand: "TestBrand",
			Name:  "TestDevice",
		}

		createdDevice, err := service.CreateDevice(ctx, input)
		require.NoError(t, err, "Failed to create device")

		err = service.DeleteDevice(ctx, createdDevice.ID)
		require.NoError(t, err, "Failed to delete device")

		_, err = service.GetDevice(ctx, createdDevice.ID)
		assert.Error(t, err, "Getting deleted device should return an error")
		assert.ErrorIs(t, err, NotFoundError, "Error should be of type NotFoundError")
	})

	t.Run("TestDeleteNonExistentDevice", func(t *testing.T) {
		ctx := context.Background()
		err := service.DeleteDevice(ctx, "non-existent-id")
		assert.Error(t, err, "Deleting non-existent device should return an error")
		assert.ErrorIs(t, err, NotFoundError, "Error should be of type NotFoundError")
	})

	t.Run("TestListDevices", func(t *testing.T) {
		ctx := context.Background()
		// Create multiple devices
		for i := 0; i < 5; i++ {
			input := CreateDeviceInput{
				Brand: "ListTestBrand",
				Name:  "ListTestDevice",
			}
			_, err := service.CreateDevice(ctx, input)
			require.NoError(t, err, "Failed to create device for list test")
		}

		params := PaginationParams{
			Limit:  10,
			Offset: 0,
		}

		result, err := service.ListDevices(ctx, params)
		require.NoError(t, err, "Failed to list devices")
		assert.GreaterOrEqual(t, len(result.Devices), 5, "Should have at least 5 devices")
		assert.GreaterOrEqual(t, result.TotalCount, int32(5), "Total count should be at least 5")
	})

	t.Run("TestSearchDevicesByBrand", func(t *testing.T) {
		ctx := context.Background()
		brandToSearch := "SearchTestBrand"
		// Create devices with the specific brand
		for i := 0; i < 3; i++ {
			input := CreateDeviceInput{
				Brand: brandToSearch,
				Name:  "SearchTestDevice",
			}
			_, err := service.CreateDevice(ctx, input)
			require.NoError(t, err, "Failed to create device for search test")
		}

		params := PaginationParams{
			Limit:  10,
			Offset: 0,
		}

		result, err := service.SearchDevicesByBrand(ctx, brandToSearch, params)
		require.NoError(t, err, "Failed to search devices by brand")
		assert.GreaterOrEqual(t, len(result.Devices), 3, "Should have at least 3 devices with the searched brand")
		for _, device := range result.Devices {
			assert.Equal(t, brandToSearch, device.Brand, "All returned devices should have the searched brand")
		}
	})
}
