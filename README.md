# Device Management API

This project is a RESTful API service for managing devices. It provides endpoints for creating, reading, updating, and deleting device records, as well as listing and searching devices.

## Technologies Used

- Go 1.22
- Gin Web Framework
- DynamoDB (AWS SDK v2)
- Docker and Docker Compose

## Prerequisites

- Go 1.22 or later
- Docker and Docker Compose
- Make

## Project Structure

    device-mgmt/
    ├── devices/
    │   ├── dynamodb.go
    │   ├── errors.go
    │   ├── input_types.go
    │   ├── service.go
    │   ├── service_int_test.go
    │   └── storage.go
    ├── Dockerfile
    ├── docker-compose.yml
    ├── go.mod
    ├── go.sum
    ├── main.go
    ├── Makefile
    ├── README.md
    └── routes_and_handlers.go

## Building the Project

To build the project, run:

    make build

This will compile the Go code and create a binary in the `bin` directory.

## Running the Service

To run the service locally:

    make run

To run the service using Docker:

    make docker-run

This will build the Docker image and start the service along with a local DynamoDB instance.

## Running Tests

To run unit tests:

    make test

To run integration tests locally:

    make test-integration

## API Endpoints

- POST /v1/devices: Create a new device
- GET /v1/devices/:id: Get a device by ID
- GET /v1/devices: List all devices (with pagination)
- PUT /v1/devices/:id: Update a device
- PATCH /v1/devices/:id: Partially update a device
- DELETE /v1/devices/:id: Delete a device
- GET /v1/devices/byBrand/:brand: Search devices by brand

## Environment Variables

- AWS_REGION: AWS region for DynamoDB (default: "eu-west-1")
- DYNAMODB_ENDPOINT: Custom endpoint for DynamoDB (used for local development)

## Development

To start development:

1. Clone the repository
2. Run `make deps` to download dependencies
3. Make your changes
4. Run tests using `make test` and `make test-integration`
5. Build and run the service using `make run` or `make docker-run`

## Cleaning Up

To clean up built binaries and test cache:

    make clean
    make test-clean

To stop and remove Docker containers:

    make docker-down
