# Go related variables
GOBASE=$(shell pwd)
GOBIN=$(GOBASE)/bin
GOFILES=$(wildcard *.go)

# Binary name
BINARY_NAME=deviceapi

# Docker related variables
DOCKER_COMPOSE=docker-compose

# Main package path
MAIN_PACKAGE_PATH=.

.PHONY: all build clean test run deps docker-build docker-up docker-down

all: test build

build:
	@echo "Building..."
	@go build -o $(GOBIN)/$(BINARY_NAME) $(MAIN_PACKAGE_PATH)

clean:
	@echo "Cleaning..."
	@go clean
	@rm -f $(GOBIN)/$(BINARY_NAME)

test:
	@echo "Testing..."
	@go test ./...

run: build
	@echo "Running..."
	@$(GOBIN)/$(BINARY_NAME)

deps:
	@echo "Downloading dependencies..."
	@go mod download

docker-build:
	@echo "Building Docker images..."
	@$(DOCKER_COMPOSE) build

docker-up:
	@echo "Starting Docker containers..."
	@$(DOCKER_COMPOSE) up -d

docker-down:
	@echo "Stopping Docker containers..."
	docker-compose stop dynamo
	@echo "Waiting for DynamoDB to stop..."
	@sleep 5
	docker-compose down

docker-logs:
	@echo "Showing Docker logs..."
	@$(DOCKER_COMPOSE) logs -f

# Build and run in Docker
docker-run: docker-build docker-up

# Rebuild the binary, rebuild Docker images, and restart containers
docker-rebuild: build docker-build docker-up

.PHONY: test-integration

# Run integration tests
test-integration:
	@echo "Starting local DynamoDB..."
	docker-compose up -d dynamo
	@echo "Waiting for DynamoDB to be ready..."
	@until docker-compose exec -T dynamo curl -s http://localhost:8000 > /dev/null; do sleep 1; done
	@echo "Running integration tests..."
	go test -v ./... -tags=integration
	@echo "Stopping local DynamoDB..."
	docker-compose stop dynamo
	@echo "Waiting for DynamoDB to stop..."
	@sleep 5
	docker-compose down

# Clean up
test-clean:
	docker-compose down
	go clean -testcache
