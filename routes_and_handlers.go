package main

import (
	"context"
	"errors"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/wI2L/fizz"
	"gitlab.com/benjamin-rood/device-mgmt/devices"
)

const (
	defaultTimeout = 10 * time.Second
	defaultLimit   = 10
	maxLimit       = 100
	// Base path for API version 1
	version = "/v1"

	devicesBasePath = "/devices"

	// Device routes
	CreateDevicePath         = devicesBasePath
	GetDevicePath            = devicesBasePath + "/:id"
	ListDevicesPath          = devicesBasePath
	UpdateDevicePath         = devicesBasePath + "/:id"
	PatchDevicePath          = devicesBasePath + "/:id"
	DeleteDevicePath         = devicesBasePath + "/:id"
	SearchDevicesByBrandPath = devicesBasePath + "/byBrand/:brand"
)

func setupRoutes(f *fizz.Fizz, service *devices.DeviceService) {
	v1 := f.Group(version, "Device API", "API for managing devices")

	v1.POST(CreateDevicePath, []fizz.OperationOption{
		fizz.Summary("Create a new device"),
		fizz.InputModel(devices.CreateDeviceInput{}),
	}, handleCreateDevice(service))

	v1.GET(GetDevicePath, []fizz.OperationOption{
		fizz.Summary("Get a device by ID"),
		fizz.InputModel(devices.GetDeviceByID{}),
	}, handleGetDevice(service))

	v1.GET(ListDevicesPath, []fizz.OperationOption{
		fizz.Summary("List all devices"),
		fizz.InputModel(devices.GetManyDevices{}),
	}, handleListDevices(service))

	v1.PUT(UpdateDevicePath, []fizz.OperationOption{
		fizz.Summary("Update a device"),
		fizz.InputModel(struct {
			devices.GetDeviceByID
			devices.UpdateDeviceInput
		}{}),
	}, handleUpdateDevice(service))

	v1.PATCH("/devices/:id", []fizz.OperationOption{
		fizz.Summary("Partially update a device"),
		fizz.InputModel(struct {
			devices.GetDeviceByID
			devices.PatchDeviceInput
		}{}),
	}, handlePatchDevice(service))

	v1.DELETE(DeleteDevicePath, []fizz.OperationOption{
		fizz.Summary("Delete a device"),
		fizz.InputModel(devices.GetDeviceByID{}),
	}, handleDeleteDevice(service))

	v1.GET(SearchDevicesByBrandPath, []fizz.OperationOption{
		fizz.Summary("Search devices by brand"),
		fizz.InputModel(devices.GetManyByBrand{}),
	}, handleSearchDevicesByBrand(service))
}

func handleCreateDevice(service *devices.DeviceService) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(c.Request.Context(), defaultTimeout)
		defer cancel()

		var input devices.CreateDeviceInput
		if err := c.ShouldBindJSON(&input); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		device, err := service.CreateDevice(ctx, input)
		if err != nil {
			handleError(c, err)
			return
		}
		c.JSON(http.StatusCreated, device)
	}
}

func handleGetDevice(service *devices.DeviceService) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(c.Request.Context(), defaultTimeout)
		defer cancel()

		var input devices.GetDeviceByID
		if err := c.ShouldBindUri(&input); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		device, err := service.GetDevice(ctx, input.ID)
		if err != nil {
			handleError(c, err)
			return
		}
		c.JSON(http.StatusOK, device)
	}
}

func handleListDevices(service *devices.DeviceService) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(c.Request.Context(), defaultTimeout)
		defer cancel()

		var input devices.GetManyDevices
		if err := c.ShouldBindQuery(&input); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		if input.Limit == 0 {
			input.Limit = defaultLimit
		} else if input.Limit > maxLimit {
			input.Limit = maxLimit
		}

		params := devices.PaginationParams{
			Limit:  input.Limit,
			Offset: input.Offset,
		}

		result, err := service.ListDevices(ctx, params)
		if err != nil {
			handleError(c, err)
			return
		}
		c.JSON(http.StatusOK, result)
	}
}

func handleUpdateDevice(service *devices.DeviceService) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(c.Request.Context(), defaultTimeout)
		defer cancel()

		var uriInput devices.GetDeviceByID
		if err := c.ShouldBindUri(&uriInput); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		var input devices.UpdateDeviceInput
		if err := c.ShouldBindJSON(&input); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		device, err := service.UpdateDevice(ctx, uriInput.ID, input)
		if err != nil {
			handleError(c, err)
			return
		}
		c.JSON(http.StatusOK, device)
	}
}

func handlePatchDevice(service *devices.DeviceService) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(c.Request.Context(), defaultTimeout)
		defer cancel()

		var uriInput devices.GetDeviceByID
		if err := c.ShouldBindUri(&uriInput); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		var input devices.PatchDeviceInput
		if err := c.ShouldBindJSON(&input); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		device, err := service.PatchDevice(ctx, uriInput.ID, input)
		if err != nil {
			handleError(c, err)
			return
		}
		c.JSON(http.StatusOK, device)
	}
}

func handleDeleteDevice(service *devices.DeviceService) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(c.Request.Context(), defaultTimeout)
		defer cancel()

		var input devices.GetDeviceByID
		if err := c.ShouldBindUri(&input); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		err := service.DeleteDevice(ctx, input.ID)
		if err != nil {
			if errors.Is(err, devices.NotFoundError) {
				c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
			}
			return
		}
		c.JSON(http.StatusNoContent, nil)
	}
}

func handleSearchDevicesByBrand(service *devices.DeviceService) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(c.Request.Context(), defaultTimeout)
		defer cancel()

		var input devices.GetManyByBrand
		if err := c.ShouldBindUri(&input); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		if err := c.ShouldBindQuery(&input); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		if input.Brand == "" {
			c.JSON(http.StatusBadRequest, gin.H{"error": "brand is required"})
			return
		}

		if input.Limit == 0 {
			input.Limit = defaultLimit
		} else if input.Limit > maxLimit {
			input.Limit = maxLimit
		}

		pagination := devices.PaginationParams{
			Limit:  input.Limit,
			Offset: input.Offset,
		}

		result, err := service.SearchDevicesByBrand(ctx, input.Brand, pagination)
		if err != nil {
			handleError(c, err)
			return
		}
		c.JSON(http.StatusOK, result)
	}
}

func handleError(c *gin.Context, err error) {
	switch {
	case errors.Is(err, devices.NotFoundError):
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
	case errors.Is(err, devices.DatabaseError):
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Database error occurred"})
	default:
		c.JSON(http.StatusInternalServerError, gin.H{"error": "An unexpected error occurred"})
	}
}
