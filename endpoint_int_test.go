//go:build integration
// +build integration

package main

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/wI2L/fizz"
	"gitlab.com/benjamin-rood/device-mgmt/devices"
)

func setupTestServer() (*fizz.Fizz, *devices.DeviceService) {
	cfg, err := config.LoadDefaultConfig(context.Background())
	if err != nil {
		panic(err)
	}

	dynamoClient := dynamodb.NewFromConfig(cfg, func(o *dynamodb.Options) {
		o.Region = "eu-west-1"
		o.BaseEndpoint = aws.String("http://localhost:8000")
	})

	store := devices.NewDynamoDBStore(dynamoClient)
	service := devices.NewDeviceService(store)

	// Ensure the table exists
	err = store.EnsureTable(context.Background())
	if err != nil {
		panic(err)
	}

	gin.SetMode(gin.TestMode)
	engine := gin.New()
	f := fizz.NewFromEngine(engine)

	// Set up routes
	setupRoutes(f, service)

	return f, service
}

func TestHTTPEndpoints(t *testing.T) {
	f, service := setupTestServer()

	t.Run("TestCreateDeviceEndpoint", func(t *testing.T) {
		input := devices.CreateDeviceInput{
			Brand: "TestBrand",
			Name:  "TestDevice",
		}
		body, _ := json.Marshal(input)

		req := httptest.NewRequest("POST", "/v1/devices", bytes.NewBuffer(body))
		req.Header.Set("Content-Type", "application/json")
		w := httptest.NewRecorder()

		f.ServeHTTP(w, req)

		assert.Equal(t, http.StatusCreated, w.Code)

		var response devices.Device
		err := json.Unmarshal(w.Body.Bytes(), &response)
		require.NoError(t, err)
		assert.NotEmpty(t, response.ID)
		assert.Equal(t, input.Brand, response.Brand)
		assert.Equal(t, input.Name, response.Name)
	})

	t.Run("TestGetDeviceEndpoint", func(t *testing.T) {
		// First, create a device
		input := devices.CreateDeviceInput{
			Brand: "TestBrand",
			Name:  "TestDevice",
		}
		device, err := service.CreateDevice(context.Background(), input)
		require.NoError(t, err)

		req := httptest.NewRequest("GET", "/v1/devices/"+device.ID, nil)
		w := httptest.NewRecorder()

		f.ServeHTTP(w, req)

		assert.Equal(t, http.StatusOK, w.Code)

		var response devices.Device
		err = json.Unmarshal(w.Body.Bytes(), &response)
		require.NoError(t, err)
		assert.Equal(t, device.ID, response.ID)
		assert.Equal(t, device.Brand, response.Brand)
		assert.Equal(t, device.Name, response.Name)
	})

	t.Run("TestGetNonExistentDeviceEndpoint", func(t *testing.T) {
		req := httptest.NewRequest("GET", "/v1/devices/non-existent-id", nil)
		w := httptest.NewRecorder()

		f.ServeHTTP(w, req)

		assert.Equal(t, http.StatusNotFound, w.Code)
	})

	t.Run("TestUpdateDeviceEndpoint", func(t *testing.T) {
		// First, create a device
		input := devices.CreateDeviceInput{
			Brand: "TestBrand",
			Name:  "TestDevice",
		}
		device, err := service.CreateDevice(context.Background(), input)
		require.NoError(t, err)

		updateInput := devices.UpdateDeviceInput{
			Brand: "UpdatedBrand",
			Name:  "UpdatedDevice",
		}
		body, _ := json.Marshal(updateInput)

		req := httptest.NewRequest("PUT", "/v1/devices/"+device.ID, bytes.NewBuffer(body))
		req.Header.Set("Content-Type", "application/json")
		w := httptest.NewRecorder()

		f.ServeHTTP(w, req)

		assert.Equal(t, http.StatusOK, w.Code)

		var response devices.Device
		err = json.Unmarshal(w.Body.Bytes(), &response)
		require.NoError(t, err)
		assert.Equal(t, device.ID, response.ID)
		assert.Equal(t, updateInput.Brand, response.Brand)
		assert.Equal(t, updateInput.Name, response.Name)
	})

	t.Run("TestUpdateNonExistentDeviceEndpoint", func(t *testing.T) {
		updateInput := devices.UpdateDeviceInput{
			Brand: "UpdatedBrand",
			Name:  "UpdatedDevice",
		}
		body, _ := json.Marshal(updateInput)

		req := httptest.NewRequest("PUT", "/v1/devices/non-existent-id", bytes.NewBuffer(body))
		req.Header.Set("Content-Type", "application/json")
		w := httptest.NewRecorder()

		f.ServeHTTP(w, req)

		assert.Equal(t, http.StatusNotFound, w.Code)
	})

	t.Run("TestPatchDeviceEndpoint", func(t *testing.T) {
		// First, create a device
		input := devices.CreateDeviceInput{
			Brand: "TestBrand",
			Name:  "TestDevice",
		}
		device, err := service.CreateDevice(context.Background(), input)
		require.NoError(t, err)

		patchInput := devices.PatchDeviceInput{
			Name: aws.String("PatchedDevice"),
		}
		body, _ := json.Marshal(patchInput)

		req := httptest.NewRequest("PATCH", "/v1/devices/"+device.ID, bytes.NewBuffer(body))
		req.Header.Set("Content-Type", "application/json")
		w := httptest.NewRecorder()

		f.ServeHTTP(w, req)

		assert.Equal(t, http.StatusOK, w.Code)

		var response devices.Device
		err = json.Unmarshal(w.Body.Bytes(), &response)
		require.NoError(t, err)
		assert.Equal(t, device.ID, response.ID)
		assert.Equal(t, device.Brand, response.Brand)
		assert.Equal(t, *patchInput.Name, response.Name)
	})

	t.Run("TestPatchNonExistentDeviceEndpoint", func(t *testing.T) {
		patchInput := devices.PatchDeviceInput{
			Name: aws.String("PatchedDevice"),
		}
		body, _ := json.Marshal(patchInput)

		req := httptest.NewRequest("PATCH", "/v1/devices/non-existent-id", bytes.NewBuffer(body))
		req.Header.Set("Content-Type", "application/json")
		w := httptest.NewRecorder()

		f.ServeHTTP(w, req)

		assert.Equal(t, http.StatusNotFound, w.Code)
	})

	t.Run("TestDeleteDeviceEndpoint", func(t *testing.T) {
		// First, create a device
		input := devices.CreateDeviceInput{
			Brand: "TestBrand",
			Name:  "TestDevice",
		}
		device, err := service.CreateDevice(context.Background(), input)
		require.NoError(t, err)

		req := httptest.NewRequest("DELETE", "/v1/devices/"+device.ID, nil)
		w := httptest.NewRecorder()

		f.ServeHTTP(w, req)

		assert.Equal(t, http.StatusNoContent, w.Code)

		// Verify the device is deleted
		_, err = service.GetDevice(context.Background(), device.ID)
		assert.Error(t, err)
	})

	t.Run("TestDeleteNonExistentDeviceEndpoint", func(t *testing.T) {
		req := httptest.NewRequest("DELETE", "/v1/devices/non-existent-id", nil)
		w := httptest.NewRecorder()

		f.ServeHTTP(w, req)

		assert.Equal(t, http.StatusNotFound, w.Code)
	})

	t.Run("TestListDevicesEndpoint", func(t *testing.T) {
		// Create multiple devices
		for i := 0; i < 5; i++ {
			input := devices.CreateDeviceInput{
				Brand: "ListTestBrand",
				Name:  "ListTestDevice",
			}
			_, err := service.CreateDevice(context.Background(), input)
			require.NoError(t, err)
		}

		req := httptest.NewRequest("GET", "/v1/devices?limit=10&offset=0", nil)
		w := httptest.NewRecorder()

		f.ServeHTTP(w, req)

		assert.Equal(t, http.StatusOK, w.Code)

		var response devices.PaginatedResult
		err := json.Unmarshal(w.Body.Bytes(), &response)
		require.NoError(t, err)
		assert.GreaterOrEqual(t, len(response.Devices), 5)
		assert.GreaterOrEqual(t, response.TotalCount, int32(5))
	})

	t.Run("TestSearchDevicesByBrandEndpoint", func(t *testing.T) {
		brandToSearch := "SearchTestBrand"
		// Create devices with the specific brand
		for i := 0; i < 3; i++ {
			input := devices.CreateDeviceInput{
				Brand: brandToSearch,
				Name:  "SearchTestDevice",
			}
			_, err := service.CreateDevice(context.Background(), input)
			require.NoError(t, err)
		}

		req := httptest.NewRequest("GET", "/v1/devices/byBrand/"+brandToSearch+"?limit=10&offset=0", nil)
		w := httptest.NewRecorder()

		f.ServeHTTP(w, req)

		assert.Equal(t, http.StatusOK, w.Code)

		var response devices.PaginatedResult
		err := json.Unmarshal(w.Body.Bytes(), &response)
		require.NoError(t, err)
		assert.GreaterOrEqual(t, len(response.Devices), 3)
		for _, device := range response.Devices {
			assert.Equal(t, brandToSearch, device.Brand)
		}
	})
}
