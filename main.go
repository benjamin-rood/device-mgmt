package main

import (
	"context"
	"log"
	"os"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/credentials"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/gin-gonic/gin"
	"github.com/wI2L/fizz"
	"github.com/wI2L/fizz/openapi"
	"gitlab.com/benjamin-rood/device-mgmt/devices"
)

func main() {
	dynamoDBEndpoint := os.Getenv("DYNAMODB_ENDPOINT")
	if dynamoDBEndpoint == "" {
		dynamoDBEndpoint = "http://localhost:8000" // Default for local development
	}

	log.Printf("Attempting to connect to DynamoDB at: %s", dynamoDBEndpoint)

	cfg, err := config.LoadDefaultConfig(context.Background(),
		config.WithRegion("eu-west-1"),
		config.WithCredentialsProvider(credentials.NewStaticCredentialsProvider("dummy", "dummy", "dummy")),
	)
	if err != nil {
		log.Fatalf("Failed to load configuration: %v", err)
	}
	if err != nil {
		log.Fatalf("Failed to load configuration: %v", err)
	}

	dynamoClient := dynamodb.NewFromConfig(cfg, func(o *dynamodb.Options) {
		o.BaseEndpoint = aws.String(dynamoDBEndpoint)
	})

	db := devices.NewDynamoDBStore(dynamoClient)

	// Ensure table exists
	if err = db.EnsureTable(context.Background()); err != nil {
		log.Fatalf("Failed to ensure Dynamo table: %v", err)
	}

	// Create DeviceService
	deviceService := devices.NewDeviceService(db)

	// Create Gin engine
	engine := gin.New()
	engine.Use(gin.Recovery())

	// Create Fizz instance
	f := fizz.NewFromEngine(engine)

	// Set up routes
	setupRoutes(f, deviceService)

	// Generate OpenAPI spec
	f.Generator().SetInfo(&openapi.Info{
		Title:       "Device API",
		Description: "API for managing devices",
		Version:     "1.0.0",
	})

	// Serve OpenAPI spec
	engine.GET("/openapi.json", func(c *gin.Context) {
		c.JSON(200, f.Generator().API())
	})

	// Start server
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	log.Printf("Server starting on port %s", port)
	if err := engine.Run(":" + port); err != nil {
		log.Fatalf("Failed to start server: %v", err)
	}
}
