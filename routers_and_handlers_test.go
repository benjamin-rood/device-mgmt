package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/benjamin-rood/device-mgmt/devices"
	"gitlab.com/benjamin-rood/device-mgmt/mocks"
)

func TestHandleCreateDevice(t *testing.T) {
	mockStore := new(mocks.Store)
	service := devices.NewDeviceService(mockStore)

	gin.SetMode(gin.TestMode)
	router := gin.New()
	router.POST(CreateDevicePath, handleCreateDevice(service))

	t.Run("successful creation", func(t *testing.T) {
		input := devices.CreateDeviceInput{
			Brand: "TestBrand",
			Name:  "TestDevice",
		}
		expectedDevice := &devices.Device{
			ID:    "test-id",
			Brand: input.Brand,
			Name:  input.Name,
		}

		mockStore.On("AddDevice", mock.Anything, &input).Return(expectedDevice, nil).Once()

		body, _ := json.Marshal(input)
		req, _ := http.NewRequest(http.MethodPost, "/devices", bytes.NewBuffer(body))
		req.Header.Set("Content-Type", "application/json")
		w := httptest.NewRecorder()

		router.ServeHTTP(w, req)

		assert.Equal(t, http.StatusCreated, w.Code)

		var response devices.Device
		err := json.Unmarshal(w.Body.Bytes(), &response)
		assert.NoError(t, err)
		assert.Equal(t, expectedDevice.ID, response.ID)
		assert.Equal(t, expectedDevice.Brand, response.Brand)
		assert.Equal(t, expectedDevice.Name, response.Name)

		mockStore.AssertExpectations(t)
	})

	t.Run("invalid input", func(t *testing.T) {
		input := devices.CreateDeviceInput{
			// Missing required fields
		}

		body, _ := json.Marshal(input)
		req, _ := http.NewRequest(http.MethodPost, "/devices", bytes.NewBuffer(body))
		req.Header.Set("Content-Type", "application/json")
		w := httptest.NewRecorder()

		router.ServeHTTP(w, req)

		assert.Equal(t, http.StatusBadRequest, w.Code)
	})

	t.Run("store error", func(t *testing.T) {
		input := devices.CreateDeviceInput{
			Brand: "TestBrand",
			Name:  "TestDevice",
		}

		mockStore.On("AddDevice", mock.Anything, &input).Return(nil, errors.New("database error")).Once()

		body, _ := json.Marshal(input)
		req, _ := http.NewRequest(http.MethodPost, "/devices", bytes.NewBuffer(body))
		req.Header.Set("Content-Type", "application/json")
		w := httptest.NewRecorder()

		router.ServeHTTP(w, req)

		assert.Equal(t, http.StatusInternalServerError, w.Code)

		mockStore.AssertExpectations(t)
	})
}

func TestHandleGetDevice(t *testing.T) {
	mockStore := new(mocks.Store)
	service := devices.NewDeviceService(mockStore)

	gin.SetMode(gin.TestMode)
	router := gin.New()
	router.GET(GetDevicePath, handleGetDevice(service))

	t.Run("successful retrieval", func(t *testing.T) {
		deviceID := "test-id"
		expectedDevice := &devices.Device{
			ID:    deviceID,
			Brand: "TestBrand",
			Name:  "TestDevice",
		}

		mockStore.On("GetDevice", mock.Anything, deviceID).Return(expectedDevice, nil).Once()

		req, _ := http.NewRequest(http.MethodGet, "/devices/"+deviceID, nil)
		w := httptest.NewRecorder()

		router.ServeHTTP(w, req)

		assert.Equal(t, http.StatusOK, w.Code)

		var response devices.Device
		err := json.Unmarshal(w.Body.Bytes(), &response)
		assert.NoError(t, err)
		assert.Equal(t, expectedDevice.ID, response.ID)
		assert.Equal(t, expectedDevice.Brand, response.Brand)
		assert.Equal(t, expectedDevice.Name, response.Name)

		mockStore.AssertExpectations(t)
	})

	t.Run("device not found", func(t *testing.T) {
		deviceID := "non-existent-id"

		mockStore.On("GetDevice", mock.Anything, deviceID).Return(nil, devices.NotFoundError).Once()

		req, _ := http.NewRequest(http.MethodGet, "/devices/"+deviceID, nil)
		w := httptest.NewRecorder()

		router.ServeHTTP(w, req)

		assert.Equal(t, http.StatusNotFound, w.Code)

		mockStore.AssertExpectations(t)
	})

	t.Run("store error", func(t *testing.T) {
		deviceID := "test-id"

		mockStore.On("GetDevice", mock.Anything, deviceID).Return(nil, errors.New("database error")).Once()

		req, _ := http.NewRequest(http.MethodGet, "/devices/"+deviceID, nil)
		w := httptest.NewRecorder()

		router.ServeHTTP(w, req)

		assert.Equal(t, http.StatusInternalServerError, w.Code)

		mockStore.AssertExpectations(t)
	})
}

func TestHandleListDevices(t *testing.T) {
	mockStore := new(mocks.Store)
	service := devices.NewDeviceService(mockStore)

	gin.SetMode(gin.TestMode)
	router := gin.New()
	router.GET(ListDevicesPath, handleListDevices(service))

	t.Run("successful listing", func(t *testing.T) {
		expectedDevices := &devices.PaginatedResult{
			Devices: []devices.Device{
				{ID: "1", Brand: "Brand1", Name: "Device1"},
				{ID: "2", Brand: "Brand2", Name: "Device2"},
			},
			TotalCount:  2,
			CurrentPage: 1,
			TotalPages:  1,
		}

		mockStore.On("ListDevices", mock.Anything, mock.AnythingOfType("devices.PaginationParams")).Return(expectedDevices, nil).Once()

		req, _ := http.NewRequest(http.MethodGet, "/devices?limit=10&offset=0", nil)
		w := httptest.NewRecorder()

		router.ServeHTTP(w, req)

		assert.Equal(t, http.StatusOK, w.Code)

		var response devices.PaginatedResult
		err := json.Unmarshal(w.Body.Bytes(), &response)
		assert.NoError(t, err)
		assert.Equal(t, expectedDevices.Devices, response.Devices)
		assert.Equal(t, expectedDevices.TotalCount, response.TotalCount)

		mockStore.AssertExpectations(t)
	})

	t.Run("store error", func(t *testing.T) {
		mockStore.On("ListDevices", mock.Anything, mock.AnythingOfType("devices.PaginationParams")).Return(nil, errors.New("database error")).Once()

		req, _ := http.NewRequest(http.MethodGet, "/devices?limit=10&offset=0", nil)
		w := httptest.NewRecorder()

		router.ServeHTTP(w, req)

		assert.Equal(t, http.StatusInternalServerError, w.Code)

		mockStore.AssertExpectations(t)
	})
}

func TestHandleUpdateDevice(t *testing.T) {
	mockStore := new(mocks.Store)
	service := devices.NewDeviceService(mockStore)

	gin.SetMode(gin.TestMode)
	router := gin.New()
	router.PUT(UpdateDevicePath, handleUpdateDevice(service))

	t.Run("successful update", func(t *testing.T) {
		deviceID := "test-id"
		input := devices.UpdateDeviceInput{
			Brand: "UpdatedBrand",
			Name:  "UpdatedDevice",
		}
		expectedDevice := &devices.Device{
			ID:    deviceID,
			Brand: input.Brand,
			Name:  input.Name,
		}

		mockStore.On("UpdateDevice", mock.Anything, deviceID, &input).Return(expectedDevice, nil).Once()

		body, _ := json.Marshal(input)
		req, _ := http.NewRequest(http.MethodPut, "/devices/"+deviceID, bytes.NewBuffer(body))
		req.Header.Set("Content-Type", "application/json")
		w := httptest.NewRecorder()

		router.ServeHTTP(w, req)

		assert.Equal(t, http.StatusOK, w.Code)

		var response devices.Device
		err := json.Unmarshal(w.Body.Bytes(), &response)
		assert.NoError(t, err)
		assert.Equal(t, expectedDevice.ID, response.ID)
		assert.Equal(t, expectedDevice.Brand, response.Brand)
		assert.Equal(t, expectedDevice.Name, response.Name)

		mockStore.AssertExpectations(t)
	})

	t.Run("device not found", func(t *testing.T) {
		deviceID := "non-existent-id"
		input := devices.UpdateDeviceInput{
			Brand: "UpdatedBrand",
			Name:  "UpdatedDevice",
		}

		mockStore.On("UpdateDevice", mock.Anything, deviceID, &input).Return(nil, devices.NotFoundError).Once()

		body, _ := json.Marshal(input)
		req, _ := http.NewRequest(http.MethodPut, "/devices/"+deviceID, bytes.NewBuffer(body))
		req.Header.Set("Content-Type", "application/json")
		w := httptest.NewRecorder()

		router.ServeHTTP(w, req)

		assert.Equal(t, http.StatusNotFound, w.Code)

		mockStore.AssertExpectations(t)
	})

	t.Run("invalid input", func(t *testing.T) {
		deviceID := "test-id"
		input := devices.UpdateDeviceInput{
			// Missing required fields
		}

		body, _ := json.Marshal(input)
		req, _ := http.NewRequest(http.MethodPut, "/devices/"+deviceID, bytes.NewBuffer(body))
		req.Header.Set("Content-Type", "application/json")
		w := httptest.NewRecorder()

		router.ServeHTTP(w, req)

		assert.Equal(t, http.StatusBadRequest, w.Code)
	})
}

func TestHandlePatchDevice(t *testing.T) {
	mockStore := new(mocks.Store)
	service := devices.NewDeviceService(mockStore)

	gin.SetMode(gin.TestMode)
	router := gin.New()
	router.PATCH(PatchDevicePath, handlePatchDevice(service))

	t.Run("successful patch", func(t *testing.T) {
		deviceID := "test-id"
		newName := "UpdatedDevice"
		input := devices.PatchDeviceInput{
			Name: &newName,
		}
		expectedDevice := &devices.Device{
			ID:    deviceID,
			Brand: "OriginalBrand",
			Name:  newName,
		}

		mockStore.On("PatchDevice", mock.Anything, deviceID, &input).Return(expectedDevice, nil).Once()

		body, _ := json.Marshal(input)
		req, _ := http.NewRequest(http.MethodPatch, "/devices/"+deviceID, bytes.NewBuffer(body))
		req.Header.Set("Content-Type", "application/json")
		w := httptest.NewRecorder()

		router.ServeHTTP(w, req)

		assert.Equal(t, http.StatusOK, w.Code)

		var response devices.Device
		err := json.Unmarshal(w.Body.Bytes(), &response)
		assert.NoError(t, err)
		assert.Equal(t, expectedDevice.ID, response.ID)
		assert.Equal(t, expectedDevice.Brand, response.Brand)
		assert.Equal(t, expectedDevice.Name, response.Name)

		mockStore.AssertExpectations(t)
	})

	t.Run("device not found", func(t *testing.T) {
		deviceID := "non-existent-id"
		newName := "UpdatedDevice"
		input := devices.PatchDeviceInput{
			Name: &newName,
		}

		mockStore.On("PatchDevice", mock.Anything, deviceID, &input).Return(nil, devices.NotFoundError).Once()

		body, _ := json.Marshal(input)
		req, _ := http.NewRequest(http.MethodPatch, "/devices/"+deviceID, bytes.NewBuffer(body))
		req.Header.Set("Content-Type", "application/json")
		w := httptest.NewRecorder()

		router.ServeHTTP(w, req)

		assert.Equal(t, http.StatusNotFound, w.Code)

		mockStore.AssertExpectations(t)
	})
}

func TestHandleDeleteDevice(t *testing.T) {
	mockStore := new(mocks.Store)
	service := devices.NewDeviceService(mockStore)

	gin.SetMode(gin.TestMode)
	router := gin.New()
	router.DELETE(DeleteDevicePath, handleDeleteDevice(service))

	t.Run("successful deletion", func(t *testing.T) {
		deviceID := "test-id"

		mockStore.On("DeleteDevice", mock.Anything, deviceID).Return(nil).Once()

		req, _ := http.NewRequest(http.MethodDelete, "/devices/"+deviceID, nil)
		w := httptest.NewRecorder()

		router.ServeHTTP(w, req)

		assert.Equal(t, http.StatusNoContent, w.Code)

		mockStore.AssertExpectations(t)
	})

	t.Run("device not found", func(t *testing.T) {
		deviceID := "non-existent-id"

		mockStore.On("DeleteDevice", mock.Anything, deviceID).Return(devices.NotFoundError).Once()

		req, _ := http.NewRequest(http.MethodDelete, "/devices/"+deviceID, nil)
		w := httptest.NewRecorder()

		router.ServeHTTP(w, req)

		assert.Equal(t, http.StatusNotFound, w.Code)

		mockStore.AssertExpectations(t)
	})
}

func TestHandleSearchDevicesByBrand(t *testing.T) {
	mockStore := new(mocks.Store)
	service := devices.NewDeviceService(mockStore)

	gin.SetMode(gin.TestMode)
	router := gin.New()
	router.GET(SearchDevicesByBrandPath, handleSearchDevicesByBrand(service))

	t.Run("successful search", func(t *testing.T) {
		brand := "TestBrand"
		expectedDevices := &devices.PaginatedResult{
			Devices: []devices.Device{
				{ID: "1", Brand: brand, Name: "Device1"},
				{ID: "2", Brand: brand, Name: "Device2"},
			},
			TotalCount:  2,
			CurrentPage: 1,
			TotalPages:  1,
		}

		mockStore.On("SearchDevicesByBrand",
			mock.Anything, // context
			brand,
			mock.AnythingOfType("devices.PaginationParams"),
		).Run(func(args mock.Arguments) {
			actualBrand := args.Get(1).(string)
			params := args.Get(2).(devices.PaginationParams)
			t.Logf("SearchDevicesByBrand called with brand: %s, limit: %d, offset: %d", actualBrand, params.Limit, params.Offset)
			assert.Equal(t, brand, actualBrand, "Brand mismatch")
		}).Return(expectedDevices, nil).Once()

		req, _ := http.NewRequest(http.MethodGet, "/devices/byBrand/"+brand+"?limit=10&offset=0", nil)
		w := httptest.NewRecorder()

		router.ServeHTTP(w, req)

		assert.Equal(t, http.StatusOK, w.Code)

		var response devices.PaginatedResult
		err := json.Unmarshal(w.Body.Bytes(), &response)
		assert.NoError(t, err)
		assert.Equal(t, expectedDevices.Devices, response.Devices)
		assert.Equal(t, expectedDevices.TotalCount, response.TotalCount)

		mockStore.AssertExpectations(t)
	})

	t.Run("invalid URL", func(t *testing.T) {
		req, _ := http.NewRequest(http.MethodGet, "/devices/byBrand/?limit=10&offset=0", nil)
		w := httptest.NewRecorder()

		router.ServeHTTP(w, req)

		assert.Equal(t, http.StatusNotFound, w.Code)
	})

	t.Run("store error", func(t *testing.T) {
		brand := "ErrorBrand"
		mockStore.On("SearchDevicesByBrand",
			mock.AnythingOfType("*context.timerCtx"),
			brand,
			mock.AnythingOfType("devices.PaginationParams"),
		).Return(nil, errors.New("database error")).Once()

		req, _ := http.NewRequest(http.MethodGet, "/devices/byBrand/"+brand+"?limit=10&offset=0", nil)
		w := httptest.NewRecorder()

		router.ServeHTTP(w, req)

		assert.Equal(t, http.StatusInternalServerError, w.Code)

		mockStore.AssertExpectations(t)
	})
}

func TestHandleError(t *testing.T) {
	gin.SetMode(gin.TestMode)

	t.Run("not found error", func(t *testing.T) {
		w := httptest.NewRecorder()
		c, _ := gin.CreateTestContext(w)
		handleError(c, devices.NewNotFoundError("Device not found"))
		assert.Equal(t, http.StatusNotFound, w.Code)
	})

	t.Run("database error", func(t *testing.T) {
		w := httptest.NewRecorder()
		c, _ := gin.CreateTestContext(w)
		handleError(c, devices.NewDatabaseError("Database connection failed"))
		assert.Equal(t, http.StatusInternalServerError, w.Code)
	})

	t.Run("unknown error", func(t *testing.T) {
		w := httptest.NewRecorder()
		c, _ := gin.CreateTestContext(w)
		handleError(c, errors.New("Unknown error"))
		assert.Equal(t, http.StatusInternalServerError, w.Code)
	})
}
